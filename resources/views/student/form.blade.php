<div class="row">
    <div class="col-md-6">
        {{Form::open(['route' => "students.store", 'method' => 'POST'])}}

        <div class="row">
            <div class="col-md-6">
                {{Form::label( 'name','Enter your name : ',['title' => 'name', 'style' => "color:black"])}}
                {{-- first value is "for" and second value is "value" --}}
            </div>

            <div class="col-md-6">
                {{Form::text('name', '',['title'=>'name here', 'class' => " mb-3", 'id' => 'myname'])

                }}
                {{-- first value is "name" second value is "value" --}}
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {{Form::label('Password : ')}}
            </div>
            <div class="col-md-6">
                {{-- {{Form::password('password',['class'=>'form-control mb-3', 'aria-label' => "Amount (to the nearest dollar)"])}} --}}
                <div class="input-group">
                    <input id="hide" type="password" class="form-control mb-3" aria-label="Amount (to the nearest dollar)" />
                    <div class="input-group-append">
                        <span id="showpass" class="input-group-text mb-3" style="cursor: pointer"><i class="fa fa-eye" ></i></span>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {{Form::label('email', 'Email : ')}}
            </div>
            <div class="col-md-6">
                {{Form::email('email','', ['class' => ' mb-3'])}}
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                {{Form::label('Hobby : ')}}
            </div>
            <div class="col-md-7">
                {{Form::label('Cricket')}}
                {{Form::checkbox('hobby', 'cricket')}}
                {{Form::label('Badminton')}}
                {{Form::checkbox('hobby', 'badminton')}}
                {{Form::label('Football')}}
                {{Form::checkbox('hobby', 'football')}}<br />
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                {{Form::label('Gender : ')}}
            </div>
            <div class="col-md-6">
                {{Form::label('Male')}}
                {{Form::radio('gender', 'male')}}
                {{Form::label('Female')}}
                {{Form::radio('gender', 'female')}}
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {{Form::label('Enrollment No. : ')}}
            </div>
            <div class="col-md-6">
                {{Form::number('enrollno', '', ['class' => ' mb-3'])}}
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {{Form::label('Date : ')}}
            </div>
            <div class="col-md-6">
                {{Form::date('date','', ['class' => ' mb-3'])}}
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {{Form::label('Subjects')}}
            </div>
            <div class="col-md-6">
                {{Form::select('subject',['maths' => 'Maths', 'science'=> "Science", 'physics' =>
                'Physics'],'',['placeholder' => 'Select any one', 'class' => 'form-control mb-3'])}}
            </div>
        </div>
    </div>
</div>


{{Form::submit('Click me',['class'=>'btn btn-primary btn-sm'])}}
{{Form::close()}}


<script>

$(function(){

    $('#showpass').on('click', function(){

        if($('#hide').attr('type') == 'text'){
            $('#hide').attr('type', 'password');
        }else{
        $('#hide').attr('type', 'text');
        }

        setTimeout(() => {
            $('#hide').attr('type', 'password');
        }, 2000);

    })


})

</script>